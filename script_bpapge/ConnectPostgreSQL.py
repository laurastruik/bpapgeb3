#!/usr/bin/python
import psycopg2
import sys
 
def main():
	#Define our connection string
	conn_string = "host='localhost' dbname='world' user='salsemgeest' password='7744'"
 
	# print the connection string we will use to connect
	print("Connecting to database\n	->%s" % (conn_string))
 
	# get a connection, if a connect cannot be made an exception will be raised here
	conn = psycopg2.connect(conn_string)
 
	# conn.cursor will return a cursor object, you can use this cursor to perform queries
	cur = conn.cursor()
	print("Connected!\n")

	# Execute a command: this creates a new table
	cur.execute("CREATE TABLE test (id serial PRIMARY KEY, num integer, data varchar);")

	# Pass data to fill a query placeholders and let Psycopg perform
	# the correct conversion (no more SQL injections!)
	cur.execute("INSERT INTO test (num, data) VALUES (%s, %s)",      (100, "abc'def"))

	# Query the database and obtain data as Python objects
	cur.execute("SELECT * FROM test;")
	cur.fetchone()
	(1, 100, "abc'def")

	# Make the changes to the database persistent
	conn.commit()

	# Close communication with the database
	cur.close()
 
if __name__ == "__main__":
	main()
	
