#!/usr/bin/python
import psycopg2
import sys

def main():
    #Define our connection string
    conn_string = "host='localhost' dbname='bpapgeb3' user='salsemgeest' password='7744'"
    print("Connecting to database\n	->%s" % (conn_string))
    global conn
    conn = psycopg2.connect(conn_string)
    global cursor
    cursor = conn.cursor()
    print("Connected!\n")



def empty_table():
    cursor.execute("DROP TABLE IF EXISTS PROTEIN_B3")
    cursor.execute("DROP TABLE IF EXISTS GENE_B3")
    cursor.execute("DROP TABLE IF EXISTS PROTEIN_GENE_B3")
    cursor.execute("DROP TABLE IF EXISTS PATHWAY_B3")
    cursor.execute("DROP TABLE IF EXISTS PROTEIN_PATHWAY_B3")



def create_tables():

    SQL_protein = """CREATE TABLE PROTEIN_B3(
    protein_id   char(20),
    protein_name varchar(100),
    length smallint,
    protein_function text,
    splicing_variant varchar(3),
    organism varchar(100),
    primary key(protein_id))
"""

    SQL_gene = """CREATE TABLE GENE_B3(
    gene_id varchar(15),
    gene_name   varchar(25),
    gene_function   text,
    cellular_location   varchar(100),
    primary key (gene_id))
"""

    SQL_protein_gene ="""CREATE TABLE PROTEIN_GENE_B3(
    gene_id varchar(15),
    protein_id char(17),
    exon    text,
    foreign key (protein_id) references PROTEIN_B3,
    foreign key (gene_id) references GENE_B3)"""

    SQL_pathway = """CREATE TABLE PATHWAY_B3(
    pathway_id   varchar(20),
    pathway_name   varchar(100),
    pathway_family   varchar(100),
    primary key (pathway_id))"""

    SQL_protein_pathway = """CREATE TABLE PROTEIN_PATHWAY_B3(
    protein_id   char(20),
    pathway_id varchar(20),
    foreign key (protein_id) references PROTEIN_B3,
    foreign key (pathway_id) references PATHWAY_B3)"""

    lijst = [SQL_protein, SQL_gene,
             SQL_pathway, SQL_protein_pathway, SQL_protein_gene]
    for x in lijst:
         cursor.execute(x)
    conn.commit()
    print("Tables created!")

if __name__ == "__main__":
    main()
    empty_table()
    create_tables()


