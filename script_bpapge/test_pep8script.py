import os
from urllib.request import urlopen

def inlezen():
    os.system("""blastn -query Bpapge_seq_B3.fa -out output_bash.txt -outfmt 6 -db nr -remote""")
    os.system("""cat output_bash.txt | awk '{if (($3 == "100.000") print $0}' > output_bash_filtered.txt""")
    os.system("""cat output_bash.txt | awk '{print$2}' > genid.txt""")
    fileout = open('genid.txt', 'r')
    ids = fileout.readlines()
    lijst = []
    for i in ids:
        newstring = i.replace("\n", "")
        lijst.append(newstring)
    return lijst



def gen_id(g_lijst):
    genids = open("genids.txt", "w")
    inhoud= ""
    for i in g_lijst:
        bestand = urlopen('http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&id=' + i + '&rettype=gb&retmode=text"')
        inhoud = bestand.readlines()
        zin1 = (str(inhoud[1]).replace("  ", "").split(" "))
        zin3 =(str(inhoud[3]).replace("  ", "").split(" "))
        zin1 = (" ".join(zin1[1:]))
        zin1 = zin1.strip(",\' \\n")
        zin3 = ("".join(zin3[-1]))
        zin3 = zin3.strip("\\n'")
        id_naam = zin3 + "\t" + zin1 + "\n"
        genids.write(id_naam)
        genids.close()
    return inhoud


def eiwit_id(e_inhoud):
    eiwitids = open("eiwitids.txt", "w")
    for line in e_inhoud:
        line = str(line)
        if "protein_id" in line:
            eiwitid = line.split("=")
            eiwitid = eiwitid[1]
            eiwitid = eiwitid.strip("\\n'\"")
        if "product" in line:
            eiwitnaam = line.split("=")
            eiwitnaam = eiwitnaam[1]
            eiwitnaam = eiwitnaam.strip("\\n'\"")
            eiwit_id_naam = eiwitid + "\t" + eiwitnaam + "\n"
        eiwitids.write(eiwit_id_naam)
    print('klaar')
    eiwitids.close()
    


def main():
    print("hallo")
    m_lijst = inlezen()
    m_inhoud = gen_id(m_lijst)
    eiwit_id(m_inhoud)

main()
    


